# window.py
#
# Copyright 2022 Haj Mousa
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Adw
from gi.repository import Gtk

@Gtk.Template(resource_path='/org/tractor/Carburetor/ui/window.ui')
class CarburetorWindow(Adw.ApplicationWindow):
    __gtype_name__ = 'CarburetorWindow'

    header_bar = Gtk.Template.Child()
    toast_overlay = Gtk.Template.Child()
    status_page = Gtk.Template.Child()
    SplitButton = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.header_bar.add_css_class("flat")
        self.SplitButton.add_css_class("suggested-action")

    def send_toast(self, text: str):
    	toast = Adw.Toast.new(text)
    	toast.set_timeout(2)
    	self.toast_overlay.add_toast(toast)

