# main.py
#
# Copyright 2022 Haj Mousa
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import sys
import gi

gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')

from gi.repository import Gtk, Gio, Adw
from gettext import gettext
from .window import CarburetorWindow


class CarburetorApplication(Adw.Application):

    def __init__(self):
        super().__init__(application_id='org.tractor.Carburetor',
                         flags=Gio.ApplicationFlags.FLAGS_NONE)
        self.create_action('quit', self.quit, ['<primary>q'])
        self.create_action('about', self.on_about_action)
        self.create_action('connect', self.on_connect)

    def do_activate(self):
        self.win = self.props.active_window
        if not self.win:
            self.win = CarburetorWindow(application=self)
        self.win.present()

    def on_about_action(self, widget, _):
    	about = Adw.AboutWindow(
    				transient_for=self.props.active_window,
    				application_name=gettext("Carburetor"),
    				application_icon="org.tractor.Carburetor",
    				developer_name=gettext("Tractor team"),
    				version="3.10",
    				developers=[
    					"دانیال بهزادی <dani.behzi@ubuntu.com>",
    					"سید حسین موسوی فرد <shmf1385@protonmail.com>",
    				],
    				issue_url="https://codeberg.org/shmf1385/Carburetor",
    				license_type=Gtk.License.GPL_3_0,
    				copyright=gettext("Copyright  2019-2022, Tractor team"))
    	about.present()

    def on_connect(self, widget, _):
    	self.win.status_page.set_icon_name("content-loading")
    	self.win.status_page.set_icon_name("security-high")
    	self.win.SplitButton.remove_css_class("suggested-action")
    	self.win.SplitButton.remove_css_class("destructive-action")
    	self.win.send_toast("Connected to 🇩🇪Germany relay")

    def create_action(self, name, callback, shortcuts=None):
        action = Gio.SimpleAction.new(name, None)
        action.connect("activate", callback)
        self.add_action(action)
        if shortcuts:
            self.set_accels_for_action(f"app.{name}", shortcuts)


def main(version):
    app = CarburetorApplication()
    return app.run(sys.argv)
